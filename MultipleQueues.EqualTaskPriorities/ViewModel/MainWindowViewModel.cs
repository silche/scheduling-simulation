﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Framework.UI.Controls;
using System.Linq;

namespace MultipleQueues.EqualTaskPriorities.ViewModel
{
    class MainWindowViewModel : ObjectBase
    {
        private Process Proc;
        private DateTime RunServiceTime;
        private RandomGenerator RandG;
        private MyQueue FirstQueue; //Первая очередь
        private MyQueue CurrentQueue; //Текущая очередь
        private MyQueue NextNotEmptyQueue; //Следующая не пустая очередь
        private MyQueue NextUnfilledQueue; //Следующая незаполненная очередь

        public ObservableCollection<MyQueue> QCollection //Коллекция очередей
        { get; set; }

        private int PId; //Номер нового процесса (Счётчик)
        private int QId; //Номер новой очереди (Счётчик)

        private DispatcherTimer timerAutoAddproc;
        private DispatcherTimer timerWorkproc;
        private DispatcherTimer timerClock;

        private TimeSpan ServiceTime; //время обработки

        public MainWindowViewModel()
        {
            //Содание экземпляров для работы с ними
            timerAutoAddproc = new DispatcherTimer();
            timerWorkproc = new DispatcherTimer();
            timerClock = new DispatcherTimer();

            timerAutoAddproc.Tick += timerAutoAddproc_Tick;
            timerAutoAddproc.Interval = new TimeSpan(0, 0, 3);

            timerWorkproc.Tick += timer_Tick;
            timerWorkproc.Interval = new TimeSpan(0, 0, 3);

            timerClock.Tick += clock_Tick;
            timerClock.Interval = new TimeSpan(0, 0, 0, 0, 50);

            RandG = new RandomGenerator();
            QCollection = new ObservableCollection<MyQueue>();
            SetInitialValues();

            WorkOff();
            timerClock.Start();
            autoAddProcess = true;
        }

        //Установка начальных значений
        private void SetInitialValues()
        {
            PId = 0;
            QueuesCount = 5;
            QueueSize = 5;
            ExecutionTimeFrom = 1;
            ExecutionTimeTo = 7;
            AutoAddFrom = 1;
            AutoAddTo = 7;
            LimitQuantTime = 4;
        }

        #region Application Description

        private string _descriptionText = "The application simulates a scheduling discipline with multiple queues and equal task priorities.\n\n" +
                                          "The application allows to monitor in real time the process of assigning resources in accordance " +
                                          "with the scheduling scheme. It generates tasks, dispatches them for execution and processes them.";

        public string DescriptionText
        {
            get { return _descriptionText; }
            set
            {
                _descriptionText = value;
                OnPropertyChanged("DescriptionText");
            }
        }

        private string _algorithmText = "The size of the queues and their number are fixed. Each task (process) is characterized by the arrival time " +
                                        "and the duration of its service.\n\n" + 
                                        "All new service requests go to the end of the first queue (i=1). A request comes in for service from " +
                                        "queue i when all queues from 1 to i-1 are empty.\n\n" + 
                                        "Tasks are withdrawn from the first queue in the order of arrival and are serviced for some quantum of time. " +
                                        "If during this time the request is completely serviced, then it leaves the system. Otherwise, an incompletely " +
                                        "serviced request goes to the end of the queue with the number i+1.\n\n" +
                                        "After servicing from queue i, the system selects for servicing a request from the non-empty queue with " +
                                        "the lowest number. Such a request may be the next request from queue i or from queue i+l " +
                                        "(provided that queue i is empty). After the time allotted to service the request from queue i, " +
                                        "the service of the request from queue 1 will begin.\n\n" +
                                        "If the system switches to service requests from queue N, they are serviced according to the FIFO discipline, " +
                                        "or by a circular algorithm.";

        public string AlgorithmText
        {
            get { return _algorithmText; }
            set
            {
                _algorithmText = value;
                OnPropertyChanged("AlgorithmText");
            }
        }

        #endregion

        #region Navigation bar

        private string _ssbarTxt;

        public string StartStopBarText
        {
            get { return _ssbarTxt; }
            set
            {
                _ssbarTxt = value;
                OnPropertyChanged("StartStopBarText");
            }
        }

        #endregion

        #region Menu bar "Home"

        //Реальное время
        private string _clocklabel;

        public string Clock
        {
            get { return _clocklabel; }
            set
            {
                _clocklabel = value;
                OnPropertyChanged("Clock");
            }
        }

        private string _ssTxt;

        public string StartStopText
        {
            get { return _ssTxt; }
            set
            {
                _ssTxt = value;
                OnPropertyChanged("StartStopText");
            }
        }

        //Cостояние программы (выполняет процессы, либо нет)
        private bool _onOffToggle;

        public bool OnOffToggle
        {
            get { return _onOffToggle; }
            set
            {
                _onOffToggle = value;
                if (_onOffToggle || !_onOffToggle) Start_Work();
                OnPropertyChanged("OnOffToggle");
            }
        }

        private string _onOfftxt;

        public string OnOffText
        {
            get { return _onOfftxt; }
            set
            {
                _onOfftxt = value;
                OnPropertyChanged("OnOffText");
            }
        }

        private bool _addButEn;

        public bool addProcButEnable
        {
            get { return _addButEn; }
            set
            {
                _addButEn = value;
                OnPropertyChanged("addProcButEnable");
            }
        }

        private bool _autoAdd;

        public bool autoAddProcess
        {
            get { return _autoAdd; }
            set
            {
                _autoAdd = value;
                OnPropertyChanged("autoAddProcess");
            }
        }

        //Текущий процесс
        private string _processLabel;

        public string Process
        {
            get { return _processLabel; }
            set
            {
                _processLabel = value;
                OnPropertyChanged("Process");
            }
        }

        //Время начала выполнения текущего процесса
        private string _runExe;

        public string StartTimeExe
        {
            get { return _runExe; }
            set
            {
                _runExe = value;
                OnPropertyChanged("StartTimeExe");
            }
        }

        //Время, оставшееся до конца выполнения текущего процесса
        private string _realTimeExe;

        public string TimeUntilEndExe
        {
            get { return _realTimeExe; }
            set
            {
                _realTimeExe = value;
                OnPropertyChanged("TimeUntilEndExe");
            }
        }

        //Счетчик количества завершенных процессов
        private int _endproccount;

        public int EndProcCount
        {
            get { return _endproccount; }
            set
            {
                _endproccount = value;
                OnPropertyChanged("EndProcCount");
            }
        }

        #endregion

        #region Панель меню "Настройки"

        //Видимость настроек в процессе работы
        private bool _displayset;

        public bool DisplaySettings
        {
            get { return _displayset; }
            set
            {
                _displayset = value;
                OnPropertyChanged("DisplaySettings");
            }
        }

        //Количество очередей
        private int _queuesCount;

        public int QueuesCount
        {
            get { return _queuesCount; }
            set
            {
                _queuesCount = value;
                OnPropertyChanged("QueuesCount");
            }
        }

        //Размер очереди
        private int _queueSize;

        public int QueueSize
        {
            get { return _queueSize; }
            set
            {
                _queueSize = value;
                OnPropertyChanged("QueueSize");
            }
        }

        //Время выполнения, значение от
        private int _exeTfrom;

        public int ExecutionTimeFrom
        {
            get { return _exeTfrom; }
            set
            {
                _exeTfrom = value;
                OnPropertyChanged("ExecutionTimeFrom");
            }
        }

        //Время выполнения, значение до
        private int _exeTto;

        public int ExecutionTimeTo
        {
            get { return _exeTto; }
            set
            {
                _exeTto = value;
                OnPropertyChanged("ExecutionTimeTo");
            }
        }

        //Время автоматического поступления, значение от
        private int _addfrom;

        public int AutoAddFrom
        {
            get { return _addfrom; }
            set
            {
                _addfrom = value;
                OnPropertyChanged("AutoAddFrom");
            }
        }

        //Время автоматического поступления, значение до
        private int _addto;

        public int AutoAddTo
        {
            get { return _addto; }
            set
            {
                _addto = value;
                OnPropertyChanged("AutoAddTo");
            }
        }

        //Предел времени квантования
        private int _quantTime;

        public int LimitQuantTime
        {
            get { return _quantTime; }
            set
            {
                _quantTime = value;
                OnPropertyChanged("LimitQuantTime");
            }
        }

        #endregion

        #region Команды

        private AsyncDelegateCommand _startstop;

        public ICommand StartStop_Click
        {
            get
            {
                if (_startstop == null)
                    _startstop = new AsyncDelegateCommand(SS_Click);
                return _startstop;
            }
        }

        private AsyncDelegateCommand _addproc;

        public ICommand AddProc_Click
        {
            get
            {
                if (_addproc == null)
                    _addproc = new AsyncDelegateCommand(AddProcess_Click);
                return _addproc;
            }
        }

        private AsyncDelegateCommand _autoaddproc;

        public ICommand AutoAddProc_Checked
        {
            get
            {
                if (_autoaddproc == null)
                    _autoaddproc = new AsyncDelegateCommand(AutoAdd_Checked);
                return _autoaddproc;
            }
        }

        #endregion

        #region Статус работы программы

        private async Task SS_Click(object sender)
        {
            //событие, возникающее при нажатии на кнопку "Launch"
            if (!OnOffToggle)
                OnOffToggle = true;
            else if (OnOffToggle)
                //если повторно нажата
                OnOffToggle = false;
            Start_Work();
        }

        private void Start_Work()
        {
            //событие, возникающее при нажатии на кнопку "Launch"
            if (OnOffToggle)
                WorkOn();
            else if (!OnOffToggle)
                //если повторно нажата
                WorkOff();
        }

        private void WorkOn()
        {
            if (QCollection.Count == 0)
            {
                CreateNewQueue(); //Создание новой очереди
                FirstQueue = QCollection[0];
            }
            //Если коллекция очередей не пуста
            else if (QCollection.Count > 0)
            {
                SetCurrentQueue();
                SetCurrentProc();
            }
            SetLabels(); //установка текстовых полей
            DisplaySettings = false;
            addProcButEnable = true; //кнопка становится "активной"
            if (autoAddProcess) //если стоит галка
                timerAutoAddproc.Start(); //начало работы таймера добавления процессов
        }

        private void WorkOff()
        {
            timerAutoAddproc.Stop();
            timerWorkproc.Stop(); //остановка таймеров
            SetLabels();
            DisplaySettings = true;
            addProcButEnable = false;
        }

        private void SetLabels()
        {
            //заполнение тектовых полей
            if (OnOffToggle)
            {
                StartStopText = "Suspend";
                OnOffText = "On";
                StartStopBarText = "Stop";
            }
            else
            {
                StartStopText = "Launch";
                OnOffText = "Off";
                StartStopBarText = "Start";
                Process = ""; //очистка текстовых полей
                StartTimeExe = "";
                TimeUntilEndExe = "";
            }
        }

        #endregion

        #region Adding a process

        private async Task AutoAdd_Checked(object sender)
        {
            if (autoAddProcess && OnOffToggle) //если стоит галка
                timerAutoAddproc.Start();
            else if (!autoAddProcess || !OnOffToggle)
                timerAutoAddproc.Stop();
        }

        private void timerAutoAddproc_Tick(object sender, EventArgs e)
        {
            //добавление процесса в очередь
            CreateNewProcess();
            // задается интенсивность добавления процессов
            timerAutoAddproc.Interval = new TimeSpan(0, 0, 0, 0,
                RandG.ServiceTimeGenerator(Convert.ToDecimal(AutoAddFrom),
                    Convert.ToDecimal(AutoAddTo)));
        }

        private bool add_click;

        private async Task AddProcess_Click(object sender)
        {
            add_click = true;
            if (QCollection.Count == 0)
            {
                CreateNewQueue(); //Создание новой очереди
                FirstQueue = QCollection[0];
            }
            //добавление процесса в очередь
            CreateNewProcess();
        }


        //Создание процесса и добавление его в очередь
        private void CreateNewProcess()
        {
            var qcount = QCollection.Count;
            ServiceTime = new TimeSpan(0, 0, 0, 0,
                    RandG.ServiceTimeGenerator(Convert.ToDecimal(ExecutionTimeFrom),
                        Convert.ToDecimal(ExecutionTimeTo)));
            Proc = new Process("Process " + PId++,
                ServiceTime, DateTime.Now);

            if (FirstQueue.ProcCollection.Count < QueueSize)
                //Добавление нового Процесса в первую очередь
                FirstQueue.ProcCollection.Add(Proc);
            else if (add_click)
                //кол-во элементов превышает заданный размер
                MessageDialog.ShowAsync(
                    "Warning",
                    "It is not possible to add a process first.\nThe number of processes exceeds the specified number.",
                    MessageBoxButton.OK);

            if (QCollection.Count == 1 && FirstQueue.ProcCollection.Count == 1)
            {
                CurrentQueue = FirstQueue;
                SetCurrentProc();
            }
            add_click = false;
        }

        #endregion

        // Создание новой очереди
        private void CreateNewQueue()
        {
            if (QCollection.Count < QueuesCount)
                QCollection.Add(new MyQueue("Queue " + QId++));
            else
                MessageDialog.ShowAsync(
                    "Warning",
                    "Unable to create a queue.\nThe number of queues exceeds the specified size.",
                    MessageBoxButton.OK);
        }

        #region Execution of processes

        //событие таймера
        private void timer_Tick(object sender, EventArgs e)
        {
            ExecutionTask();
        }

        //Выполнение задачи (процесса), эмуляция работы
        private void ExecutionTask()
        {

            CurrentQueue.DeleteFirst(); //Удаляем завершенный процесс
            EndProcCount++; //Увеличиваем количество выполненных процессов

            //Если есть пустые очереди, не считая первую, удаляем их
            while (CurrentQueue.ProcCollection.Count == 0 && CurrentQueue == QCollection[QCollection.Count - 1] &&
                   CurrentQueue != FirstQueue)
            {
                CurrentQueue = QCollection[QCollection.Count - 2];
                QCollection.RemoveAt(QCollection.Count - 1);
            }
            CurrentQueue = null;
            SetCurrent();
        }

        #endregion

        #region Установки

        //Задание текущего процесса
        private void SetCurrentProc()
        {

            if (CurrentQueue != null) //Если текущая очередь не пуста
            {
                if (CurrentQueue.ProcCollection.Count != 0)
                {
                    var CurrentProc = CurrentQueue.GetFirst();
                    timerWorkproc.Interval = CurrentProc.Duration;
                    timerWorkproc.Start();

                    if (Process != CurrentProc.ProcessName) //отображение информации о процессе
                    {
                        Process = CurrentProc.ProcessName;
                        Process = CurrentProc.ProcessName;
                        RunServiceTime = DateTime.Now;
                        StartTimeExe = RunServiceTime.ToString("HH:mm:ss:f");
                    }
                }
            }
            else
            {
                timerWorkproc.Stop();
                Process = "";
                StartTimeExe = "";
                TimeUntilEndExe = "";
            }
        }

        //Поиск и задание текущей очереди с наивысшим приоритетом
        private void SetCurrentQueue()
        {
            if (QCollection.Count > 0)
                //Поиск и задание текущей очереди если в первой очереди нет процессов
                if (FirstQueue.ProcCollection.Count == 0)
                {
                    //Если в коллекции не только одна очередь
                    if (QCollection.Count > 1)
                    {
                        NextNotEmptyQueue = null;
                        if ((NextNotEmptyQueue = QCollection.FirstOrDefault(q => q.ProcCollection.Count != 0)) != null)
                            CurrentQueue = NextNotEmptyQueue;
                    }
                }
                else
                    CurrentQueue = FirstQueue;
        }

        void SetCurrent()
        {
            //Ищем не пустую очередь и задаем её в виде текущей
            SetCurrentQueue();
            //Задаём процесс на обработку из текущей очереди
            SetCurrentProc();
        }

        #endregion

        #region Real-time information display

        private void clock_Tick(object sender, EventArgs e)
        {
            //таймер часов
            //Clock = DateTime.Now.ToString("HH:mm:ss:f\ndd.MM.yy");
            Clock = DateTime.Now.ToString("HH:mm:ss:f");
            if (OnOffToggle && CurrentQueue != null && CurrentQueue.ProcCollection.Count > 0)
                TimeUntilEnd();
        }

        //Подсчет оставшегося времени
        private void TimeUntilEnd()
        {
            TimeUntilEndExe = String.Format("{0:0.00}",
                (TimeEndProc(CurrentQueue.GetFirst().Duration.TotalMilliseconds) - (DateTime.Now)).TotalSeconds);

            TimeSpan timeExec = TimeEndProc(LimitQuantTime * 1000) - DateTime.Now;
            //Время, оставшееся до конца выполнения процесса
            TimeSpan timeUntilEndProcExecu = TimeEndProc(CurrentQueue.GetFirst().Duration.TotalMilliseconds) -
                                             DateTime.Now;


            //Если время, отведённое на выполнение уже истекло, а процесс ещё выполняется
            if (timeExec.TotalMilliseconds <= 0 && timeUntilEndProcExecu.TotalMilliseconds > 0)
            {
                if (QCollection.Count > 1)
                {
                    //Если текущая очередь это последняя очередь (и не первая)
                    if (CurrentQueue == QCollection[QCollection.Count - 1])
                    {
                        //Если количество очередей есть конечное заданное количество
                        if (QCollection.Count == QueuesCount)
                            //Тогда выполняется FIFO до конца
                            return;
                        else
                        {
                            //Иначе создаём новую очередь и переносим в неё незавершенный процесс
                            CreateNewQueue();
                            TranFromToNextQueue(QCollection[QCollection.Count - 1]);
                        }
                    }

                    //Если текущая очередь это первая или любая следующая, но не последняя
                    else
                    {
                        //Если у первой найденной очереди есть свободное место и найденная явл. следующей по отношению к текущей
                        if (
                            (NextUnfilledQueue =
                                QCollection.FirstOrDefault(q => (q.ProcCollection.Count < QueueSize
                                                                 &&
                                                                 (QCollection.IndexOf(q) >
                                                                  QCollection.IndexOf(CurrentQueue))))) !=
                            null)
                        {
                            //Неуспевающий завершиться процесс переносим в найденную очередь
                            TranFromToNextQueue(NextUnfilledQueue);
                            NextUnfilledQueue = null;
                        }

                        //Если такая очередь не найдена
                        else if (NextUnfilledQueue == null)
                        {
                            //Если количество очередей меньше заданного количества
                            if (QCollection.Count < QueuesCount)
                            {
                                //Создаём новую очередь и переносим в неё незавершенный процесс
                                CreateNewQueue();
                                TranFromToNextQueue(QCollection[QCollection.Count - 1]);
                            }
                            //Иначе продолжаем выполнять процесс до победного
                            else
                            {
                                //Если все очереди заполнены
                                if (QCollection.FirstOrDefault(q => (q.ProcCollection.Count < QueueSize)) == null)
                                {
                                    timerAutoAddproc.Stop();
                                    autoAddProcess = false;
                                    MessageDialog.ShowAsync(
                                        "Error",
                                        "Unable to move the element. All queues are full.\nStops the automatic addition of tasks.",
                                        MessageBoxButton.OK);
                                }
                                else
                                {
                                    MessageDialog.ShowAsync(
                                        "Warning",
                                        "Unable to move the element. The next queue(s) is(are) full.",
                                        MessageBoxButton.OK);
                                }
                                return;
                            }
                        }
                    }
                }
                //Если в коллекции только одна очередь
                else
                {
                    //Создаём новую очередь и переносим в неё незавершенный процесс
                    CreateNewQueue();
                    TranFromToNextQueue(QCollection[QCollection.Count - 1]);
                }

                //Задание текущей очереди и текущего процесса 
                SetCurrent();

                //Задаём процесс на обработку из текущей очереди
                SetCurrentProc();
            }
        }

        //Перемещение процесса в указанную очередь
        private void TranFromToNextQueue(MyQueue qNext)
        {
            qNext.ProcCollection.Add(
                CurrentQueue.GetFirst());
            CurrentQueue.DeleteFirst();
        }

        //Вычисление времени окончания выполнения процесса
        private DateTime TimeEndProc(double serviceTime)
        {
            return RunServiceTime.AddMilliseconds(serviceTime);
        }

        #endregion
    }
}