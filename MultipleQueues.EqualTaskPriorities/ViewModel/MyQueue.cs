﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace MultipleQueues.EqualTaskPriorities
{
    class MyQueue
    {
        public ObservableCollection<Process> ProcCollection { get; set; }// объявление переменной  
        
        public string QueueName { get; set; }//Имя очереди

        public MyQueue(string queuename)
        {//создание экземпляра 
            ProcCollection = new ObservableCollection<Process>();
            QueueName = queuename;
        }

        public void Add(Process proc)
        {//метод добавления объекта 
            ProcCollection.Add(proc);
        }

        public void Clear()
        {//метод, очищающий 
            while (ProcCollection.Count > 0)
                DeleteFirst();
        }

        public void DeleteFirst()
        {//метод, удаляющий верхний элемент 
            ProcCollection.RemoveAt(0);
        }

        public Process GetFirst()
        {//метод, возвращающий первый элемент 
            return ProcCollection[0];
        }
        public Process GetLast()
        {//метод, возвращающий последний элемент 
            return ProcCollection.Last();
        }
        
        //Перемещение первого элемента в конец очереди
        public void FirsttoEnd(TimeSpan TimeRemainingProc)
        {
            GetFirst().Duration = TimeRemainingProc;
            var buf = GetFirst();//запоминаем первый процесс
            for (int i = 0; i < ProcCollection.Count - 1; i++)
                ProcCollection[i] = ProcCollection[i + 1];
            ProcCollection[ProcCollection.Count - 1] = buf;
        }
    }
}
