﻿using System.ComponentModel;

namespace MultipleQueues.EqualTaskPriorities.ViewModel
{
    class ObjectBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,new PropertyChangedEventArgs(propName));
        }
    }
}
