﻿using System;

namespace MultipleQueues.EqualTaskPriorities
{
    class Process
    {
        public string ProcessName { get; set; }

        public TimeSpan Duration { get; set; }

        public DateTime TimeofEntry { get; set; }
        
        //Конструктор класса, принимающий несколько аргументов
        public Process(string processname, TimeSpan duration, DateTime entrytime)
        {
            ProcessName = processname; //Задается имя процесса
            Duration = duration; //задается время обработки
            TimeofEntry = entrytime; //Задается время поступления
        }
    }
}
