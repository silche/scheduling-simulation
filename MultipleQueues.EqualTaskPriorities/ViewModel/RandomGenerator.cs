﻿using System;
using System.Collections.Generic;

namespace MultipleQueues.EqualTaskPriorities
{
    internal class RandomGenerator //класс, реализующий необходимые методы
    {
        //для работы с генератором случайных чисел в данном проекте
        private static Random rn = new Random();

        private int sec = 1000; //секунда, выраженная в миллисекундах
        private int sleepTime; //время паузы
        private int numberareas;
        private List<int> listCriticalAreas;
        
        //метод, задающий время паузы, исходя из интервала
        public int ServiceTimeGenerator(decimal from, decimal to)
        {
            sleepTime = 0; //сброс времени паузы
            sleepTime = rn.Next((int)From(from), (int)To(to));
            return sleepTime;
        }

        //начало интервала в миллисекундах
        private decimal From(decimal from)
        {
            return from*sec;
        }

        //конец интервала в миллисекундах
        private decimal To(decimal to)
        {
            return to*sec;
        }
        public int DurationCriticalArea(int duration)
        {
            return rn.Next(duration);
        }
        public bool RandomBool()
        {
            if (rn.Next(10) > 5)
                return true;
            else
                return false;
        }
/*
        public int NumberCriticalAreas(int percent)
        {
            return rn.Next(1, (int) (numberareas*percent/100));
        }
*/

    }
}