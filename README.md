# Scheduling Simulation

The application simulates a scheduling discipline with multiple queues and equal task priorities.

Bachelor's coursework project of the 3rd year in the discipline "System software".

© 06/2016

<div align="center">
<img alt="Main view" src="https://gitlab.com/silche/scheduling-simulation/-/raw/master/screenshots/multiple_queues_qual_task_priorities/main_view.png"
    title="Main view" width="68%">
<p><em>Main view</em></p>
</div>

#### Description

The application allows to monitor in real time the process of assigning resources in accordance with the scheduling scheme. It generates tasks, dispatches them for execution and processes them.

#### Algorithm
The size of the queues and their number are fixed. Each task (process) is characterized by the arrival time and the duration of its service.

All new service requests go to the end of the first queue (i=1). A request comes in for service from queue i when all queues from 1 to i-1 are empty.

Tasks are withdrawn from the first queue in the order of arrival and are serviced for some quantum of time. If during this time the request is completely serviced, then it leaves the system. Otherwise, an incompletely serviced request goes to the end of the queue with the number i+1.

After servicing from queue i, the system selects for servicing a request from the non-empty queue with the lowest number. Such a request may be the next request from queue i or from queue i+1 (provided that queue i is empty). After the time allotted to service the request from queue i, the service of the request from queue 1 will begin.

If the system switches to service requests from queue N, they are serviced according to the FIFO discipline, or by a circular algorithm.

<details><summary>More screenshots</summary>
<div align="center">
<img alt="Settings" src="https://gitlab.com/silche/scheduling-simulation/-/raw/master/screenshots/multiple_queues_qual_task_priorities/settings.png"
    title="Settings" width="68%">
<p><em>Settings</em></p>
<img alt="Description popup" src="https://gitlab.com/silche/scheduling-simulation/-/raw/master/screenshots/multiple_queues_qual_task_priorities/description_pop_up_window.png"
    title="Description popup" width="68%">
<p align="center"><em>Description popup</em></p>
</div>
</details>
